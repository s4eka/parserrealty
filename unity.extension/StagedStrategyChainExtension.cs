﻿using System;
using Unity.Storage;
using Unity.Strategies;

namespace unity.extension
{
    public static class StagedStrategyChainExtension
    {
        // Methods
        public static void AddNew2<TStrategy, TStageEnum>(this IStagedStrategyChain<BuilderStrategy, TStageEnum> chain, TStageEnum stage) where TStrategy : BuilderStrategy, new()
        {
            chain.Add(Activator.CreateInstance<TStrategy>(), stage);
        }
    }


}
