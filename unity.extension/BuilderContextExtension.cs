﻿using Unity;
using Unity.Builder;
using Unity.Resolution;

namespace unity.extension
{
    public static class BuilderContextExtension
    {
        public static void AddResolverOverrides(this BuilderContext context, params ResolverOverride[] overrides)
        {
            context.AddResolverOverrides(overrides);
        }

        public static TResult NewBuildUp2<TResult>(this IUnityContainer context)
        {
            return context.NewBuildUp2<TResult>(null);
        }

        public static TResult NewBuildUp2<TResult>(this IUnityContainer context, string name)
        {
            return (TResult)context.BuildUp(typeof(TResult), name, null);
        }
    }
}
