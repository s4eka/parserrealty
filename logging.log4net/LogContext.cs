﻿using log4net;
using logging.core;
using System;

namespace logging.log4net
{
    public class LogContext : ILogContext
    {
        private static readonly ILog _logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public LogContext()
        {
        }
        #region // ------------------- Error ------------------- //
        public void Error(string errorMessage)
        {
            _logger.Error(errorMessage);
        }

        public void Error(string message, string url)
        {
            _logger.ErrorFormat(message, url);
        }

        public void Error(string title, Exception exception)
        {
            _logger.Error(title, exception);
        }

        public void Error(Exception exception)
        {
            _logger.Error(string.Empty, exception);
        }

        public void ErrorFormat(Exception exception, string format, params object[] parameterList)
        {
            _logger.Error( string.Format(format, parameterList), exception);
        }

        public void ErrorFormat(string format, params object[] parameterList)
        {
            _logger.ErrorFormat(format, parameterList);
        }
        #endregion

        #region // ------------------- Info  ------------------- //
        public void Info(string message)
        {
            _logger.Info(message);
        }

        public void InfoFormat(string format, params object[] parameterList)
        {
            _logger.InfoFormat(format, parameterList);
        }
        #endregion

        #region // ------------------- Trace ------------------- //
        public void Trace(string message)
        {
            _logger.Debug(message);
        }

        public void TraceFormat(string format, params object[] parameterList)
        {
            _logger.DebugFormat(format, parameterList);
        }
        #endregion
    }
}
