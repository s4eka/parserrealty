﻿using common.shared;
using HtmlAgilityPack;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;

namespace task.parser.neagent
{
    public class RentAdvertCaster : ACaster
    {
        public override bool TryGetAddress(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                var address = divNode.SelectSingleNode(".//div[@class='imd_mid']//div[@class='md_head']//em").InnerText;
                if (string.IsNullOrEmpty(address)) return false;
                advert.Address = address;
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public override bool TryGetAgent(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            var oldUrl = driver.Url;
            try
            {
                driver.Navigate().GoToUrl(url);
                driver.FindElementByClassName("phone_show").Click();
                Thread.Sleep(5000);
                var phonesText = driver.FindElementByXPath("//div[@class='c_wr']//div[@class='cphone']").Text;
                var nameText = driver.FindElementByXPath("//div[@class='c_wr']//div[@class='cname']").Text;

                advert.Agent = new Agent
                {
                    IsAgent = false,
                    Name = nameText,
                    Phones = phonesText.Split(",")
                };
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                driver.Navigate().GoToUrl(oldUrl);
            }
            return true;
        }

        public override bool TryGetCity(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            advert.City = "г. Минск";
            return true;
        }

        public override bool TryGetCountry(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            advert.Country = "Беларусь";
            return true;
        }

        public override bool TryGetDate(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                var dateRegexp = new Regex(@"\d{4}-\d{2}-\d{2} \d{2}:\d{2}");
                var dateStr = pageNode.SelectSingleNode("//div[text()='Обновлено:']/following-sibling::div").InnerText.Trim();
                var dateText = string.Empty;
                if (dateRegexp.IsMatch(dateStr))
                {
                    dateText = dateRegexp.Match(dateStr).Value;
                }
                else
                {
                    dateText = $"{DateTime.Now.ToString("yyyy-MM-dd")} { new Regex(@"\d{2}:\d{2}").Match(dateStr).Value}";
                }
                var date = DateTime.ParseExact(dateText, "yyyy-MM-dd HH:mm", null);
                advert.Date = date;
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public override bool TryGetDescription(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                var description = pageNode
                    .SelectSingleNode("//div//div//div[@class='text-content']//p")
                    .InnerText;
                advert.Description = description;
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public override bool TryGetFloor(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            { 
                var floor = int.Parse(new Regex(@"\d{1,3}").Matches(pageNode.SelectSingleNode("//div[text()='Этаж:']/following-sibling::div").InnerText).FirstOrDefault().Value);
                if (floor < 0 && floor > 1000) return false;
                advert.Floor = floor;
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public override bool TryGetImages(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                var images = pageNode.SelectNodes("//img[@data-big-src]").Select(el => el.Attributes["data-big-src"].Value).Where(image => !image.Contains("/2.jpg")).ToList();
                advert.Images = images;
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public override bool TryGetLocation(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                var locations = new Regex(@"\d{1,3}\.\d{1,10},\d{1,3}\.\d{1,10}")
                    .Match(pageNode.SelectSingleNode("//div[@class='map-margin241']//script[contains(text(),'m_params')]").InnerText)
                    .Value
                    .Split(',')
                    .Select(match => decimal.Parse(match))
                    .ToList();
                advert.Lat = locations[0];
                advert.Long = locations[1];
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public override bool TryGetPriceAndCurrency(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            var oldUrl = driver.Url;
            try
            {
                var priceText = divNode.SelectSingleNode(".//div//div[@class='itm_price']").InnerText.Replace(" ", "").Replace("руб.","");
                var price = double.Parse((new Regex(@"\d{1,8}(\.\d{1,2}){0,1}").Match(priceText).Value));
                advert.Currency = "BYN";
                advert.Price = price;
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                driver.Navigate().GoToUrl(oldUrl);
            }
            return true;
        }

        public override bool TryGetRegion(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            advert.Region = "Минская область";
            return true;
        }

        public override bool TryGetRooms(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                var rooms = int.Parse(new Regex(@"\d{1,3}")
                    .Matches(pageNode.SelectSingleNode("//div[text()='Комнат:']/following-sibling::div").InnerText).FirstOrDefault().Value);
                advert.Rooms = rooms;
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public override bool TryGetSquares(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                var squareRegex = new Regex(@"\d{1,3}(\.\d){0,1} \/ \d{1,3}(\.\d){0,1} \/ \d{1,3}(\.\d){0,1}");
                var squareEl = squareRegex.Match(pageNode.SelectSingleNode("//div[text()='Площадь:']/following-sibling::div").InnerText).Value;
                var squareArray = squareEl.Split('/');
                advert.Square = double.Parse(squareArray[0]);
                advert.LivingSquare = double.Parse(squareArray[1]);
                double.TryParse(squareArray.Length >= 3 ? squareArray[2] : "0", out var kitchenSq);
                advert.KitchenSquare = kitchenSq;
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }
    }
}
