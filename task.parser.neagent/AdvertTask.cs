﻿using common.shared;
using HtmlAgilityPack;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Unity;

namespace task.parser.neagent
{
    public class HeaderTask : AParser
    {
        [Dependency]
        public RentAdvertCaster RentAdvertCaster { get; set; }

        protected override ACaster Caster => RentAdvertCaster;

        public async Task Parse()
        {
            Parse(
                @"https://neagent.by",
                "Rent",
                "Flat");
        }

        protected override string GetUrlFromDiv(HtmlNode htmlNode) => htmlNode.SelectSingleNode("//a[@class='a_more ']").Attributes["href"].Value;

        protected override string GetNextUrl(RemoteWebDriver driver) => 
            driver.FindElementsByLinkText(">").SingleOrDefault(element => element.GetAttribute("rel").Equals("next")).GetAttribute("href");

        protected override IEnumerable<IWebElement> GetDivsFromElement(RemoteWebDriver driver) => driver.FindElementsByClassName("imd "); 
        
    }
}
