﻿using common.shared;
using System;
using Unity;
using unity.eventbrokerextension;
using System.Threading.Tasks;
using task.timer;

namespace task.parser.neagent
{
    public class TaskInitializer
    {
        [Dependency]
        public HeaderTask HeaderTask { get; set; }
        
        [InjectionConstructor]
        public TaskInitializer()
        {

        }

        [SubscribesTo(EventName.OnStartService)]
        public async Task StartService(object sender, EventArgs eventArgs)
        {
            TimerOperation.AddTask(eventArgs, HeaderTask.GetType(), HeaderTask.Parse);
        }


    }
}
