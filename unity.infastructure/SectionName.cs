﻿namespace unity.infrastructure
{
    public abstract class SectionName
    {
        public const string Subscribers = "subscribers";
        public const string Tasks = "tasksection";
        public const string Services = "services";

    }
}
