﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using FluentValidation;
using Unity.AutoRegistration;
using unity.core;
using unity.eventbrokerextension;
using Unity;
using Unity.Injection;
using Unity.Lifetime;
using Microsoft.Practices.Unity.Configuration;

namespace unity.infrastructure
{
    public static class Bootstrapper
    {
        private static readonly Lazy<IUnityContainer> _container = new Lazy<IUnityContainer>(BulidUnityContainer);

        public static IUnityContainer BuildContainer()
        {
            return Container;
        }

        public static IUnityContainer Container
        {
            get {
                return _container.Value;
            }
        }

        #region Methods

        private static IUnityContainer BulidUnityContainer()
        {
            IUnityContainer container = new UnityContainer();

            container.AddNewExtension<EventBrokerExtension>();

            string executingPath = Path.GetDirectoryName(new Uri((Assembly.GetExecutingAssembly().CodeBase)).AbsolutePath);

            //EventLog.WriteEntry(string.Format("Bootstrapper.BuildContainer() Started {0}", executingPath), new StackTrace(true).ToString());

            container.AddExtension(new Diagnostic());
            var section = (UnityConfigurationSection)ConfigurationManager.GetSection("unity");
            List<string> assemblyNames = section.Assemblies.Select(assembly => Path.Combine(executingPath, $"{assembly.Name}.dll")).ToList();

            container
                .ConfigureAutoRegistration()
                .ExcludeSystemAssemblies()
                .ExcludeAssemblies(a => !assemblyNames.Select(Path.GetFileNameWithoutExtension).Contains(a.GetName().Name))
                .LoadAssembliesFrom(assemblyNames)

                .Include(t => t.DecoratedWith<WithNameAttribute>(),
                    Then.Register()
                    .AsITypeNameMatchesWithName(x => x.GetAttribute<WithNameAttribute>().Name)
                    .UsingSingletonMode())

                .Include(If.ImplementsITypeName,
                    Then.Register()
                    .AsITypeNameMatches()
                    .UsingSingletonMode())

                .Include(t => t.DecoratedWith<DependencyAttribute>() && !t.IsInterface && !t.IsAbstract,
                    Then.Register()
                    .WithTypeName()
                    .UsingSingletonMode())


                .Include(t => t.ImplementsITypeNameWithoutPostfix(WellKnownAppParts.Entity),
                    Then.Register()
                        .AsITypeNameWithoutPostfix(WellKnownAppParts.Entity)
                        .WithInjection(new InjectionMember[] { new InjectionConstructor() })
                        .UsingPerCallMode())

                .Include(t => t.ImplementsITypeNameWithPostfix(WellKnownAppParts.Context),
                    Then.Register()
                        .AsITypeNameWithPostfix(WellKnownAppParts.Context)
                        .WithInjection(new InjectionMember[] { new InjectionConstructor() })
                        .UsingPerCallMode())

                .Include(t => t.ImplementsITypeNameFilterByPostfix(new[] { WellKnownAppParts.DataTransferObject, WellKnownAppParts.Builder, WellKnownAppParts.Model }),
                    Then.Register()
                        .UsingPerCallMode())

                .Include(t => t.ImplementsITypeNameFilterByPostfix(new[] { WellKnownAppParts.ModelBuilder }),
                    Then.Register()
                        .UsingSingletonMode())

                //Container.RegisterType<IValidator<Model>, CustomValidator>(new ContainerControlledLifetimeManager());
                //.Include(If.DecoratedWith<ValidatorAttribute>,
                // (t, c) =>
                // {
                //     ValidatorAttribute attribute = t.GetCustomAttributes(false).FirstOrDefault(a => a.GetType() == typeof(ValidatorAttribute)) as ValidatorAttribute;
                //     if (attribute != null)
                //         c.RegisterType(typeof(IValidator<>).MakeGenericType(t), (attribute.ValidatorType), new PerThreadLifetimeManager());
                // })

                .Include(t => t.DecoratedMethodWith<SubscribesToAttribute>() && !t.IsInterface && !t.IsAbstract,
                    Then.Register()
                            .UsingSingletonMode()
                        .AutoResolve())
               .ApplyAutoRegistration();

            //EventLog.WriteEntry(string.Format("Bootstrapper.BuildContainer() End {0}", executingPath), string.Empty);
            return container;
        }

        public static void CleanUp()
        {
            if (Container != null)
            {
                Container.Dispose();
            }
        }

        #endregion
    }
}