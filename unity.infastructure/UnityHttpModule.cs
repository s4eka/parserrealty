﻿//using System;
//using System.Web;
//using System.Web.UI;
//using Unity;

//namespace unity.infrastructure
//{
//    public class UnityHttpModule : IHttpModule
//    {
//        #region IHttpModule Members

//        public void Init(HttpApplication context)
//        {
//            context.PreRequestHandlerExecute += OnPreRequestHandlerExecute;
//        }

//        public void Dispose()
//        {
//        }

//        #endregion

//        private void OnPreRequestHandlerExecute(object sender, EventArgs e)
//        {
//            IHttpHandler currentHandler = HttpContext.Current.Handler;
//            if (currentHandler == null) return;
//            if (currentHandler is DefaultHttpHandler) return;
//            try
//            {
//                Bootstrapper.Container.BuildUp(currentHandler.GetType(), currentHandler);
//            }
//            catch (Exception ex)
//            {
//                throw new InvalidOperationException(string.Format("Unable to Build up Unity container: '{0}'", ex.Message));
//            }

//            var currentPage = HttpContext.Current.Handler as Page;
//            if (currentPage != null)
//            {
//                currentPage.PreLoad += Page_PreLoad;
//            }
//        }

//        private void Page_PreLoad(object sender, EventArgs e)
//        {
//            var currentPage = (Page) sender;
//            if (currentPage.HasControls())
//                BuildUpControls(currentPage.Controls);
//        }

//        private void BuildUpControls(ControlCollection controls)
//        {
//            IUnityContainer container = Bootstrapper.Container;
//            foreach (Control control in controls)
//            {
//                if (control is UserControl)
//                    try
//                    {
//                        container.BuildUp(control.GetType(), control);
//                    }
//                    catch (Exception ex)
//                    {
//                        throw new InvalidOperationException(string.Format("Unable to Build up Unity container: '{0}'", ex.Message));
//                    }
//                if (control.HasControls())
//                    BuildUpControls(control.Controls);
//            }
//        }
//    }
//}