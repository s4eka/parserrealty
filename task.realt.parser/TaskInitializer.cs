﻿using common.shared;
using System;
using Unity;
using unity.eventbrokerextension;
using System.Threading.Tasks;
using task.timer;

namespace task.parser.realt
{
    public class TaskInitializer
    {
        [Dependency]
        public RealtAdvertTask RealtAdvertTask { get; set; }
        
        [InjectionConstructor]
        public TaskInitializer()
        {

        }

        [SubscribesTo(EventName.OnStartService)]
        public async Task StartService(object sender, EventArgs eventArgs)
        {
            TimerOperation.AddTask(eventArgs, 
                RealtAdvertTask.GetType(), 
                () => RealtAdvertTask.Start("Sale", "Flat", @"https://realt.by/sale/flats/?view=0#tabs"), 
                @"https://realt.by/sale/flats/?view=0#tabs");

            TimerOperation.AddTask(eventArgs,
                RealtAdvertTask.GetType(),
                () => RealtAdvertTask.Start("Rent", "Flat", @"https://realt.by/rent/flat-for-long/?view=0#tabs"),
                @"https://realt.by/rent/flat-for-long/?view=0#tabs");
        }
    }
}
