﻿using common.shared;
using HtmlAgilityPack;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;

namespace task.parser.realt
{
    public class RealtAdvertTask : AParser
    {
        [Dependency]
        public RealtCaster RealtCaster { get; set; }

        protected override ACaster Caster => RealtCaster;

        public async Task Start(string contractType, string realtyType, string siteUrl)
        {
            Parse(siteUrl, contractType, realtyType);
        }

        protected override IEnumerable<IWebElement> GetDivsFromElement(RemoteWebDriver driver) =>
            driver.FindElementsByXPath("//div[@class='bd-table-item vip-item']");


        protected override string GetNextUrl(RemoteWebDriver driver) =>
            $"{driver.FindElementByXPath("//div[@class='uni-paging']//a[@class='active']/following-sibling::a").GetAttribute("href")}&view=0";

        protected override string GetUrlFromDiv(HtmlNode htmlNode) => htmlNode.SelectSingleNode(".//div[@class='ad']//a").Attributes["href"].Value;
    }
}
