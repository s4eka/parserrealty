﻿using common.helper;
using common.shared;
using HtmlAgilityPack;
using OpenQA.Selenium.Remote;
using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace task.parser.realt
{
    public class RealtCaster : ACaster
    {
        public override bool TryGetAddress(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                var address = divNode.SelectSingleNode(".//div[@class='ad']//a").InnerText;
                if (string.IsNullOrEmpty(address)) return false;
                advert.Address = address;
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public override bool TryGetAgent(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                var phones = pageNode.SelectNodes("//td[text()='Телефоны']/following-sibling::td//div//strong//p").Select(el => el.InnerText).ToList();

                var agent = pageNode.SelectSingleNode("//td[text()='Агентство']")?.InnerText;
                var isAgent = true;
                if (string.IsNullOrEmpty(agent))
                {
                    isAgent = false;
                    agent = "Контактное лицо";
                }
                var name = pageNode.SelectSingleNode($"//td[text()='{agent}']/following-sibling::td")?.InnerText;
                if (string.IsNullOrEmpty(name))
                {
                    name = pageNode.SelectSingleNode($"//td[text()='Специалист']/following-sibling::td//div//div[@class='contacts']//span")?.InnerText;
                }
                var email = string.Empty;
                try
                {
                    email = pageNode.SelectSingleNode("//td[text()='E-mail']/following-sibling::td")?.InnerText;
                    if (string.IsNullOrEmpty(email))
                        email = pageNode.SelectSingleNode("//td[text()='Емаил']/following-sibling::td")?.InnerText;
                } catch(Exception e) { }
                advert.Agent = new Agent
                {
                    IsAgent = isAgent,
                    Name = name,
                    Phones = phones,
                    Email = email
                };
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public override bool TryGetCity(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                var city = pageNode.SelectSingleNode("//td[text()='Населенный пункт']/following-sibling::td/a/strong").InnerText;
                if (!city.Contains("Минск")) return false;
                advert.City = "г. Минск";
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public override bool TryGetCountry(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            advert.Country = "Беларусь";
            return true;
        }

        public override bool TryGetDate(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                var dateRegexp = new Regex(@"\d{4}-\d{2}-\d{2} \d{2}:\d{2}");
                var dateStr = divNode.SelectSingleNode(".//div[@class='date']//span").InnerText;
                var date = DateTime.ParseExact(dateStr, "dd.MM.yyyy", CultureInfo.InvariantCulture);
                advert.Date = date;
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public override bool TryGetDescription(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                var description = divNode
                    .SelectSingleNode(".//div[@class='text']//p").InnerText;

                advert.Description = description;
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public override bool TryGetFloor(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                var floor = int.Parse(new Regex(@"\d{1,3}").Matches(divNode.SelectSingleNode(".//div[@class='ee']//span").InnerText).FirstOrDefault().Value);
                if (floor < 0 && floor > 1000) return false;
                advert.Floor = floor;
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public override bool TryGetImages(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                var images = pageNode.SelectNodes("//div[@class='photo-item']//a").Select(el => el.Attributes["href"].Value).ToList();

                advert.Images = images;
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public override bool TryGetLocation(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                var locationText = pageNode.SelectSingleNode("//div[@id='map-center']").Attributes["data-center"].Value;
                var locations = new Regex(@"\d{1,2}\.\d{1,10}").Matches(locationText).Select(match => decimal.Parse(match.Value)).ToList();
                
                advert.Long = locations[0];
                advert.Lat = locations[1];
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public override bool TryGetPriceAndCurrency(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                var priceText = divNode
                    .SelectSingleNode(".//div[@class='cena']//span")
                    .Attributes["data-840"].Value.Replace("&amp;nbsp;", "").Replace(" & nbsp;", "").Replace(" ", "");

                double.TryParse(
                    new Regex(@"\d{1,8}(\.\d{1,2}){0,1}").Match(priceText).Value,
                    out var price);

                if (price.Equals(default(double))) return false;

                var currency = priceText.Replace(price.ToString(), "").Replace("/месяц", "");

                advert.Currency = CurrencyHelper.Currencies[currency];
                advert.Price = price;
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public override bool TryGetRegion(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            advert.Region = "Минская область";
            return true;
        }

        public override bool TryGetRooms(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                var rooms = int.Parse(new Regex(@"\d{1,2}")
                    .Match(pageNode.SelectSingleNode("//td[text()='Комнат всего/разд.']/following-sibling::td").SelectSingleNode(".//strong").InnerText).Value);

                advert.Rooms = rooms;
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public override bool TryGetSquares(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                var squareRegex = new Regex(@"\d{1,3}(\.\d){0,1} \/ \d{0,3}(\.\d){0,1} \/ (-|\d{0,3}(\.\d){0,1})");
                string[] squareArray = null;
                try
                {
                    squareArray = squareRegex.Match(pageNode.SelectNodes(".//tr//td[@class='table-row-right']").FirstOrDefault(el => squareRegex.IsMatch(el.InnerText)).InnerText).Value.Replace(",", ".").Split('/');
                }
                catch (Exception e)
                {
                }
                advert.Square = double.Parse(squareArray[0]);
                advert.LivingSquare = double.Parse(squareArray[1]);
                double.TryParse(squareArray.Length >= 3 ? squareArray[2] : "0", out var kitchenSq);
                advert.KitchenSquare = kitchenSq;
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }
    }
}
