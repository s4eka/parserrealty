﻿using System;
using System.Threading.Tasks;

namespace unity.eventbroker
{
    public delegate Task TaskEventHandler(object sender, EventArgs e);
}
