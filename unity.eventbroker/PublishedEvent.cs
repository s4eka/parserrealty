﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;

namespace unity.eventbroker
{
    public class PublishedEvent
    {
        private List<object> publishers;
        private SortedList<int, TaskEventHandler> subscribers;
        
        public PublishedEvent()
        {
            publishers = new List<object>();
            subscribers = new SortedList<int, TaskEventHandler>(Comparer<int>.Create((first, last) => first.CompareTo(last) == 0 ? 1 : first.CompareTo(last)));
        }

        public IEnumerable<object> Publishers
        {
            get
            {
                foreach(object publisher in publishers)
                {
                    yield return publisher;
                }
            }
        }

        public IEnumerable<TaskEventHandler> Subscribers
        {
            get
            {
                foreach(var subscriber in subscribers.Values)
                {
                    yield return subscriber;
                }
            }
        }

        public bool HasPublishers
        {
            get { return publishers.Count > 0; }
        }

        public bool HasSubscribers
        {
            get { return subscribers.Count > 0; }
        }
        
        public void AddPublisher(object publisher, string eventName)
        {
            publishers.Add(publisher);
            var targetEvent = publisher.GetType().GetEvent(eventName);
            GuardEventExists(eventName, publisher, targetEvent);

            var addEventMethod = targetEvent.GetAddMethod();
            GuardAddMethodExists(targetEvent);

            TaskEventHandler newSubscriber = OnPublisherFiring;
            addEventMethod.Invoke(publisher, new object[] { newSubscriber });
        }

        public void RemovePublisher(object publisher, string eventName)
        {
            publishers.Remove(publisher);
            var targetEvent = publisher.GetType().GetEvent(eventName);
            GuardEventExists(eventName, publisher, targetEvent);

            var removeEventMethod = targetEvent.GetRemoveMethod();
            GuardRemoveMethodExists(targetEvent);

            TaskEventHandler subscriber = OnPublisherFiring;
            removeEventMethod.Invoke(publisher, new object[] {subscriber});
        }

        public void AddSubscriber(TaskEventHandler subscriber, int orderNumber)
        {
            subscribers.Add(orderNumber, subscriber);
        }

        public void RemoveSubscriber(TaskEventHandler subscriber)
        {
            subscribers.RemoveAt(subscribers.IndexOfValue(subscriber));
        }

        private async Task OnPublisherFiring(object sender, EventArgs e)
        {
            foreach(var subscriber in subscribers.Values)
            {
                await subscriber(sender, e);

                //await Task.Run(() =>  subscriber(sender, e));
                //subscriber(sender, e);
            }
        }

        private static void GuardEventExists(string eventName, object publisher, EventInfo targetEvent)
        {
            if(targetEvent == null)
            {
                throw new ArgumentException(string.Format("The event '{0}' is not implemented on type '{1}'",
                                                          eventName, publisher.GetType().Name));
            }
        } 

        private static void GuardAddMethodExists(EventInfo targetEvent)
        {
            if(targetEvent.GetAddMethod() == null)
            {
                throw new ArgumentException(string.Format("The event '{0}' does not have a public Add method",
                                                          targetEvent.Name));
            }
        }

        private static void GuardRemoveMethodExists(EventInfo targetEvent)
        {
            if (targetEvent.GetRemoveMethod() == null)
            {
                throw new ArgumentException(string.Format("The event '{0}' does not have a public Remove method",
                                                          targetEvent.Name));
            }
        }
    }
}
