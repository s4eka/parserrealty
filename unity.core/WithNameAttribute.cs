﻿using System;

namespace unity.core
{
    [AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = false)]
    public class WithNameAttribute : Attribute
    {
        public string Name;
        public WithNameAttribute(string name)
        {
            Name = name;
        }
    }
}
