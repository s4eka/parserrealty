﻿using Newtonsoft.Json;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;

namespace common.shared
{
    public abstract class AParser
    {
        protected abstract ACaster Caster { get; }

        public void Parse
               (
               string siteUrl,
               string contractType,
               string realtyType)
        {
            Console.OutputEncoding = Encoding.UTF8;
            var attempt = 10;
            while (attempt-- > 0)
            {
                string nextUrl = null;
                var host = ConfigurationManager.AppSettings["selenium.host"];
                using (var driver = new RemoteWebDriver(new Uri(host), new ChromeOptions()))
                {
                    try
                    {
                        driver.Navigate().GoToUrl(siteUrl);
                        do
                        {
                            var divs = GetDivsFromElement(driver)
                                .Select(div =>
                                {
                                    var form = new HtmlAgilityPack.HtmlDocument();
                                    form.LoadHtml(div.GetAttribute("innerHTML"));
                                    return form;
                                })
                                .Skip(1)
                                .ToList();
                            foreach (var formNode in divs)
                            {
                                try
                                {
                                    var url = GetUrlFromDiv(formNode.DocumentNode);
                                    var pageNode = GetHtmlNode(url, true);
                                    var header = CastAdvert(driver, formNode.DocumentNode, pageNode, url, contractType, realtyType);
                                    //Console.WriteLine($"advert parsed: {url}");
                                    Set(header);
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e);
                                }
                            }
                            nextUrl = GetNextUrl(driver);
                            driver.Navigate().GoToUrl(nextUrl);
                            nextUrl = GetNextUrl(driver);
                        } while (!string.IsNullOrEmpty(nextUrl));
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                    finally
                    {
                        driver.Close();
                    }
                }
            }
        }
        protected ParsedAdvert CastAdvert(RemoteWebDriver driver,
           HtmlAgilityPack.HtmlNode divNode,
           HtmlAgilityPack.HtmlNode pageNode,
           string url,
           string contractType,
           string realtyType)
        {
            var advert = new ParsedAdvert
            {
                ContractType = contractType,
                RealtyType = realtyType,
                Url = url
            };

            if (!Caster.TryGetAddress(driver, divNode, pageNode, url, advert) ||
            !Caster.TryGetImages(driver, divNode, pageNode, url, advert) ||
            !Caster.TryGetAgent(driver, divNode, pageNode, url, advert) ||
            !Caster.TryGetCity(driver, divNode, pageNode, url, advert) ||
            !Caster.TryGetCountry(driver, divNode, pageNode, url, advert) ||
            !Caster.TryGetDate(driver, divNode, pageNode, url, advert) ||
            !Caster.TryGetDescription(driver, divNode, pageNode, url, advert) ||
            !Caster.TryGetFloor(driver, divNode, pageNode, url, advert) ||
            !Caster.TryGetLocation(driver, divNode, pageNode, url, advert) ||
            !Caster.TryGetPriceAndCurrency(driver, divNode, pageNode, url, advert) ||
            !Caster.TryGetRegion(driver, divNode, pageNode, url, advert) ||
            !Caster.TryGetRooms(driver, divNode, pageNode, url, advert) ||
            !Caster.TryGetSquares(driver, divNode, pageNode, url, advert)) return null;

            if (advert.Rooms > 10 ||
                advert.Rooms < 1 ||
                advert.Floor > 45 ||
                advert.Floor < 1)
                throw new Exception("shit advert");
           
            return advert;
        }

        protected abstract string GetUrlFromDiv(HtmlAgilityPack.HtmlNode htmlNode);

        protected abstract string GetNextUrl(RemoteWebDriver driver);

        protected abstract IEnumerable<IWebElement> GetDivsFromElement(RemoteWebDriver driver);
        
       
        private HtmlAgilityPack.HtmlNode GetHtmlNode(string urlAddress, bool withJS = false)
        {
            var html = GetHtml(urlAddress, withJS);
            var doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(html);
            return doc.DocumentNode;
        }

        private string GetHtml(string urlAddress, bool withScripts = false)
        {
            if (withScripts)
            { 
                var host = ConfigurationManager.AppSettings["selenium.host"];
                using (var driver = new RemoteWebDriver(new Uri(host), new ChromeOptions()))
                {
                    driver.Navigate().GoToUrl(urlAddress);
                    Thread.Sleep(1000);
                    return driver.FindElementByTagName("html").GetAttribute("innerHTML");
                }
            }

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(urlAddress);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream receiveStream = response.GetResponseStream();
                StreamReader readStream = null;

                if (response.CharacterSet == null)
                {
                    readStream = new StreamReader(receiveStream);
                }
                else
                {
                    readStream = new StreamReader(receiveStream, Encoding.GetEncoding(response.CharacterSet));
                }

                string data = readStream.ReadToEnd();

                response.Close();
                readStream.Close();
                return data;
            }
            return null;
        }

        public void Set(ParsedAdvert advert)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["api.host"]);
            var postData = JsonConvert.SerializeObject(advert);
            var data = Encoding.UTF8.GetBytes(postData);

            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            httpWebRequest.ContentLength = data.Length;

            using (var streamWriter = httpWebRequest.GetRequestStream())
            {
                streamWriter.Write(data, 0, data.Length);
            }
            var response = (HttpWebResponse)httpWebRequest.GetResponse();

            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            Console.WriteLine(responseString);
        }
    }
}
