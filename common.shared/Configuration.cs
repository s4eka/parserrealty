﻿namespace common.shared
{
    public class Configuration
    {
        public const string TaskParserNeagent                   = "[task].[parser].[neagent]";
        public const string TaskParserRealt                     = "[task].[parser].[realt]";
        public const string TaskParserGohome                    = "[task].[parser].[gohome]";
        public const string TaskParserGohomeRent                = "[task].[parser].[gohome].[rent]";
        public const string TaskParserDomovitoSale              = "[task].[parser].[domovito].[sale]";
        public const string TaskParserDomovitoRent              = "[task].[parser].[domovito].[rent]";
        public const string TaskParserTS                        = "[task].[parser].[ts]";

        public const string TaskParserOnlinerRent               = "[task].[parser].[onliner].[rent]";
        
        public const string TaskMapAddressAddress               = "[task].[map].[address].[address]";
        public const string TaskParserRealtRent = "[task].[parser].[realt].[rent]"; 



    }

}
