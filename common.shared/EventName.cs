﻿namespace common.shared
{
    public abstract class EventName
    {
        public const string OnStartService = "event://on.start.service";

        public const string OnWallet = "event://on.wallet";
        public const string OnBlock = "event://on.block";
        public const string OnBlockHeader = "event://on.block.header";
        public const string OnTransaction = "event://on.trasnasction";
    }
}
