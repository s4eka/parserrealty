﻿using OpenQA.Selenium.Remote;

namespace common.shared
{
    public abstract class ACaster
    {
        public abstract bool TryGetImages(RemoteWebDriver driver,
            HtmlAgilityPack.HtmlNode divNode,
            HtmlAgilityPack.HtmlNode pageNode,
            string url,
            ParsedAdvert advert);

        public abstract bool TryGetFloor(RemoteWebDriver driver,
            HtmlAgilityPack.HtmlNode divNode,
            HtmlAgilityPack.HtmlNode pageNode,
            string url,
            ParsedAdvert advert);

        public abstract bool TryGetLocation(RemoteWebDriver driver,
            HtmlAgilityPack.HtmlNode divNode,
            HtmlAgilityPack.HtmlNode pageNode,
            string url,
            ParsedAdvert advert);

        public abstract bool TryGetDescription(RemoteWebDriver driver,
            HtmlAgilityPack.HtmlNode divNode,
            HtmlAgilityPack.HtmlNode pageNode,
              string url,
            ParsedAdvert advert);

        public abstract bool TryGetSquares(RemoteWebDriver driver,
            HtmlAgilityPack.HtmlNode divNode,
            HtmlAgilityPack.HtmlNode pageNode,
            string url,
            ParsedAdvert advert);

        public abstract bool TryGetRooms(RemoteWebDriver driver,
            HtmlAgilityPack.HtmlNode divNode,
            HtmlAgilityPack.HtmlNode pageNode,
            string url,
            ParsedAdvert advert);

        public abstract bool TryGetPriceAndCurrency(RemoteWebDriver driver,
            HtmlAgilityPack.HtmlNode divNode,
            HtmlAgilityPack.HtmlNode pageNode,
            string url,
            ParsedAdvert advert);

        public abstract bool TryGetDate(RemoteWebDriver driver,
            HtmlAgilityPack.HtmlNode divNode,
            HtmlAgilityPack.HtmlNode pageNode,
            string url,
            ParsedAdvert advert);

        public abstract bool TryGetCity(RemoteWebDriver driver,
            HtmlAgilityPack.HtmlNode divNode,
            HtmlAgilityPack.HtmlNode pageNode,
            string url,
            ParsedAdvert advert);

        public abstract bool TryGetRegion(RemoteWebDriver driver,
            HtmlAgilityPack.HtmlNode divNode,
            HtmlAgilityPack.HtmlNode pageNode,
            string url,
           ParsedAdvert advert);

        public abstract bool TryGetCountry(RemoteWebDriver driver,
            HtmlAgilityPack.HtmlNode divNode,
            HtmlAgilityPack.HtmlNode pageNode,
            string url,
            ParsedAdvert advert);

        public abstract bool TryGetAgent(RemoteWebDriver driver,
            HtmlAgilityPack.HtmlNode divNode,
            HtmlAgilityPack.HtmlNode pageNode,
            string url,
            ParsedAdvert advert);

        public abstract bool TryGetAddress(RemoteWebDriver driver,
            HtmlAgilityPack.HtmlNode divNode,
            HtmlAgilityPack.HtmlNode pageNode,
            string url,
            ParsedAdvert advert);

    }
}
