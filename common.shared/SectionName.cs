﻿namespace common.shared
{
    public abstract class Section
    {
        public const string Tasks = "tasksection";
        public const string Services = "services";
    }
}
