﻿using common.shared;
using HtmlAgilityPack;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using System.Collections.Generic;
using System.Threading.Tasks;
using Unity;

namespace task.parser.domovita
{
    public class AdvertTask : AParser
    {
        [Dependency]
        public Caster DomovitaCaster { get; set; }

        protected override common.shared.ACaster Caster => DomovitaCaster;

        public async Task Start(string contractType, string realtyType, string siteUrl)
        {
            Parse(siteUrl,
                contractType,
                realtyType);
        }

        protected override IEnumerable<IWebElement> GetDivsFromElement(RemoteWebDriver driver) =>
            driver.FindElementsByXPath("//table[@class='table fs-14 table_view items']//tbody//tr");

        protected override string GetNextUrl(RemoteWebDriver driver) =>
            driver.FindElementByXPath("//a[text()='Следующая']").GetAttribute("href");

        protected override string GetUrlFromDiv(HtmlNode htmlNode) =>
            htmlNode.SelectSingleNode(".//td[@class='address']//a").Attributes["href"].Value;
    }
}
