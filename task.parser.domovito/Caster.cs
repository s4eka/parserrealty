﻿using common.helper;
using common.shared;
using HtmlAgilityPack;
using OpenQA.Selenium.Remote;
using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace task.parser.domovita
{
    public class Caster : ACaster
    {
        public override bool TryGetAddress(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                var address = pageNode.SelectSingleNode("//div[@class='object-info__parametr']//span[contains(text(),'Адрес')]/following-sibling::span").InnerText;
                if (string.IsNullOrEmpty(address)) return false;
                advert.Address = address;
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public override bool TryGetAgent(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                var phones = pageNode.SelectNodes("//div[@class='owner-info__phone']//a").Select(el => el.Attributes["href"].Value.Replace("tel:", ""));

                var email = pageNode.SelectSingleNode("//div[@class='owner-info__email']//a").InnerText;

                var isAgentText = pageNode.SelectSingleNode("//div[@class='owner-info__status']")?.InnerText;
                var isAgent = true;
                var name = string.Empty;
                if (!string.IsNullOrEmpty(isAgentText))
                {
                    name = pageNode.SelectSingleNode("//div[@class='owner-info__name']").InnerText;
                    isAgent = !isAgentText.Contains("Собственник");
                }
                else
                    name = pageNode.SelectSingleNode("//div[@class='owner-info__agency']//a").InnerText;
                
                advert.Agent = new Agent
                {
                    IsAgent = isAgent,
                    Name = name,
                    Phones = phones,
                    Email = email
                };
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public override bool TryGetCity(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                var city = pageNode.SelectSingleNode("//span[@id='city']").InnerText;
                if (!city.Contains("Минск")) return false;
                
                advert.City = "г. Минск";
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public override bool TryGetCountry(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            advert.Country = "Беларусь";
            return true;
        }

        public override bool TryGetDate(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                var dateStr = pageNode.SelectSingleNode("//span[@class='publication-info__item publication-info__update-date']").InnerText;
                var date = DateTime.ParseExact(new Regex(@"\d{1,2}\.\d{1,2}\.\d{4}").Match(dateStr).Value, "dd.MM.yyyy", CultureInfo.InvariantCulture);
                advert.Date = date;
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public override bool TryGetDescription(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                var description = pageNode
                    .SelectSingleNode("//div[@id='object-description']//div//p")?.InnerText ?? string.Empty;

                advert.Description = description;
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public override bool TryGetFloor(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                var floor = int.Parse(
                    new Regex(@"\d{1,3}")
                        .Match(pageNode.SelectSingleNode("//div[@class='object-info__parametr']//span[contains(text(),'Этаж') and not(contains(text(),'Этажность'))]/following-sibling::span").InnerText)
                        .Value);
                if (floor < 0 && floor > 1000) return false;
                advert.Floor = floor;
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public override bool TryGetImages(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                var images = pageNode
                    .SelectNodes("//li//a//img")
                    .Select(el => el.Attributes["src"].Value).Where(href => !string.IsNullOrEmpty(href)).ToList();

                advert.Images = images;
            }
            catch (Exception e)
            {
            }
            return true;
        }

        public override bool TryGetLocation(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                var locationText = pageNode.SelectSingleNode("//div[@id='map']").Attributes["data-coords"].Value;
                var locations = new Regex(@"\d{1,2}\.\d{1,10}").Matches(locationText).Select(match => decimal.Parse(match.Value)).ToList();

                advert.Long = locations[1];
                advert.Lat = locations[0];
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public override bool TryGetPriceAndCurrency(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                var priceText = pageNode
                    .SelectSingleNode("//div[@class='dropdown-menu dropdown-menu-right calculator']//div[position()=1]")
                    .InnerText.Replace("nbsp;", "").Replace(" ", "").Replace("&", "");

                if (advert.ContractType.Equals("Rent") && !priceText.Contains("$/мес")) return false;
                double.TryParse(
                    new Regex(@"\d{1,8}(\.\d{1,2}){0,1}").Match(priceText).Value,
                    out var price);
                if (price.Equals(default(double))) return false;

                var currency = priceText.Replace(price.ToString(), "").Replace("/мес.", "");
                
                advert.Currency = CurrencyHelper.Currencies[currency];
                advert.Price = price;
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public override bool TryGetRegion(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            advert.Region = "Минская область";
            return true;
        }

        public override bool TryGetRooms(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                var rooms = int.Parse(new Regex(@"\d{1,2}")
                    .Match(pageNode.SelectSingleNode("//div[@class='object-info__parametr']//span[contains(text(),'Комнат')]/following-sibling::span/a").InnerText).Value);

                advert.Rooms = rooms;
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public override bool TryGetSquares(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                var digitRegex = new Regex(@"\d{1,3}(\.\d){0,1}");
                var squareText = pageNode.SelectSingleNode("//span[contains(text(),'Общая площадь')]/following-sibling::span").InnerText;
                var livingText = pageNode.SelectSingleNode("//span[contains(text(),'Жилая площадь')]/following-sibling::span").InnerText;
                var kitchenText = pageNode.SelectSingleNode("//span[contains(text(),'Кухня')]/following-sibling::span").InnerText;
                advert.Square = double.Parse(digitRegex.Match(squareText).Value);
                advert.LivingSquare = double.Parse(digitRegex.Match(livingText).Value);
                advert.KitchenSquare = double.Parse(digitRegex.Match(kitchenText).Value);
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

    }
}
