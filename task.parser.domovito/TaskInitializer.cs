﻿using common.shared;
using System;
using Unity;
using unity.eventbrokerextension;
using System.Threading.Tasks;
using task.timer;

namespace task.parser.domovita
{
    public class TaskInitializer
    {
        [Dependency]
        public AdvertTask AdvertTask { get; set; }
        
        [InjectionConstructor]
        public TaskInitializer()
        {

        }

        [SubscribesTo(EventName.OnStartService)]
        public async Task StartService(object sender, EventArgs eventArgs)
        {
            TimerOperation.AddTask(eventArgs,
                AdvertTask.GetType(), 
                () => AdvertTask.Start("Sale", "Flat", @"https://domovita.by/minsk/flats/sale?show_type=table&order=date_revision&page=%7B0%7D"),
                @"https://domovita.by/minsk/flats/sale?show_type=table&order=date_revision&page=%7B0%7D");

            TimerOperation.AddTask(eventArgs,
                AdvertTask.GetType(),
                () => AdvertTask.Start("Rent", "Flat", @"https://domovita.by/minsk/flats/rent?show_type=table&order=date_revision&page=%7B0%7D"),
                @"https://domovita.by/minsk/flats/rent?show_type=table&order=date_revision&page=%7B0%7D");
        }


    }
}
