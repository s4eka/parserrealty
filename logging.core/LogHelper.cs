﻿using System;

namespace logging.core
{
    public class LogHelper<T> : IDisposable where T: ILogContext
    {
        private T _logger;
        private DateTime _startTime;
        private string _sectionName;

        public LogHelper(T logger, string sectionName, params object[] args)
        {
            _logger = logger;
            _startTime = DateTime.UtcNow;
            _sectionName = string.Format(sectionName, args);

            _logger.TraceFormat("->: {0};", _sectionName);
        }

        public void Trace(string format, params object[] args)
        {
            _logger.TraceFormat("--: {0}; {1};", _sectionName, string.Format(format, args));
        }

        public void Info(string format, params object[] args)
        {
            _logger.InfoFormat(format, args);
        }

        public void Error(string format, params object[] args)
        {
            _logger.Error(string.Format(format, args));
        }

        public void Error(Exception ex, string format, params object[] args)
        {
            _logger.Error(string.Format("--: {0}; {1};", _sectionName, string.Format(format, args)), ex);
        }

        public void Error(Exception ex)
        {
            _logger.Error(string.Format("--: {0}", _sectionName), ex);
        }

        public void Dispose()
        {
            _logger.InfoFormat("<-: {0}; duration: {1}", _sectionName, DateTime.UtcNow.Subtract(_startTime).ToString(@"mm\:ss\:ffffff"));
        }
    }
}
