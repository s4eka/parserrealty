﻿using System;

namespace logging.core
{
    public interface ILogContext
    {
        void Error(string errorMessage);
        void Error(string message, string url);

        void Error(string title, Exception exception);

        void Error(Exception exception);
        
        void ErrorFormat(string format, params object[] parameterList);

        void ErrorFormat(Exception exception, string format, params object[] parameterList);

        void Info(string message);

        void InfoFormat(string format, params object[] parameterList);

        void Trace(string message);

        void TraceFormat(string format, params object[] parameterList);
    }
}
