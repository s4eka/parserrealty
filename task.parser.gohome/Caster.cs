﻿using common.helper;
using common.shared;
using HtmlAgilityPack;
using OpenQA.Selenium.Remote;
using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace task.parser.gohome
{
    public class Caster : ACaster
    {
        public override bool TryGetAddress(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                var address = pageNode.SelectSingleNode("//li[@class='li-feature']//div[contains(text(),'Улица, дом:')]/following-sibling::div").InnerText;
                if (string.IsNullOrEmpty(address)) return false;
                advert.Address = address;
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public override bool TryGetAgent(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                var phones = pageNode
                    .SelectNodes("//div[@class='w-phone']//a[@class='phone__link']")
                    .Where(el => el.Attributes["href"].Value.Contains("tel"))
                    .Select(el => el.Attributes["href"].Value.Replace("tel:", ""));

                var email = pageNode.SelectSingleNode("//div[@class='w-phone']//a[@class='phone__link email']").InnerText;

                var isAgent = !pageNode.SelectSingleNode("//div[@class='username']//span").InnerText.Contains("обственник");
                var name = string.Empty;
                if (!isAgent)
                    name = pageNode.SelectSingleNode("//div[@class='username']").InnerText.Replace("Собственник", "").Replace("собственник", "").Replace(" ", "");
                else
                    name = pageNode.SelectSingleNode("//div[@class='username']//span//a")?.InnerText;

                name = name.Replace("Визбранное", "").Replace("\r", "").Replace("\n", "");

                advert.Agent = new Agent
                {
                    IsAgent = isAgent,
                    Name = name,
                    Phones = phones,
                    Email = email
                };
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public override bool TryGetCity(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                advert.City = "г. Минск";
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public override bool TryGetCountry(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            advert.Country = "Беларусь";
            return true;
        }

        public override bool TryGetDate(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                var dateStr = pageNode.SelectSingleNode("//li[@class='li-feature']//div[contains(text(),'Дата добавления:')]/following-sibling::div").InnerText;
                var date = DateTime.ParseExact(new Regex(@"\d{1,2}\.\d{1,2}\.\d{4}").Match(dateStr).Value, "dd.MM.yyyy", CultureInfo.InvariantCulture);
                advert.Date = date;
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public override bool TryGetDescription(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                var description = pageNode
                    .SelectSingleNode("//article//p[@itemprop='description']")?.InnerText ?? string.Empty;

                advert.Description = description;
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public override bool TryGetFloor(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                var floor = int.Parse(
                    new Regex(@"\d{1,3}")
                        .Matches(divNode.SelectSingleNode(".//td[@class='td-floor']//b[contains(text(),'из')]").InnerText)
                        .FirstOrDefault()
                        .Value);
                if (floor < 0 && floor > 1000) return false;
                advert.Floor = floor;
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public override bool TryGetImages(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                var images = pageNode.SelectNodes("//div[@class='slide']//a[@class='fancybox block__link']").Select(el => $"https://gohome.by{el.Attributes["href"].Value}")
                    .Where(href => !string.IsNullOrEmpty(href))
                    .ToList();
                advert.Images = images;
            }
            catch (Exception e)
            {
            }
            return true;
        }

        public override bool TryGetLocation(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                var latitude = pageNode.SelectSingleNode("//input[@id='map_latitude']").Attributes["value"].Value;
                var longitude = pageNode.SelectSingleNode("//input[@id='map_longitude']").Attributes["value"].Value;

                advert.Long = decimal.Parse(longitude);
                advert.Lat = decimal.Parse(latitude);
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public override bool TryGetPriceAndCurrency(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                var priceText = divNode
                    .SelectSingleNode(".//td[@class='td-price total']//div[@class='price bigest']")
                    .InnerText.Replace(" ", "");

                if (priceText.Contains("договорная")) return false;
                double.TryParse(
                    new Regex(@"\d{1,8}(\.\d{1,2}){0,1}").Match(priceText).Value,
                    out var price);
                if (price.Equals(default(double))) return false;

                var currency = priceText.Replace(price.ToString(), "");

                advert.Currency = CurrencyHelper.Currencies[currency];
                advert.Price = price;
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public override bool TryGetRegion(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            advert.Region = "Минская область";
            return true;
        }

        public override bool TryGetRooms(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                var rooms = int.Parse(new Regex(@"\d{1,2}")
                    .Match(pageNode.SelectSingleNode("//div[@class='description']//a[contains(text(),'комнатная')]").InnerText).Value);

                advert.Rooms = rooms;
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public override bool TryGetSquares(RemoteWebDriver driver, HtmlNode divNode, HtmlNode pageNode, string url, ParsedAdvert advert)
        {
            try
            {
                var digitRegex = new Regex(@"\d{1,3}(\.\d){0,1}");
                var squareText = pageNode.SelectSingleNode("//div[text()='Площадь общая:']/following-sibling::div[@class='description']").InnerText;
                var livingText = pageNode.SelectSingleNode("//div[text()='Площадь общая:']/following-sibling::div[@class='description']").InnerText;
                var kitchenText = pageNode.SelectSingleNode("//div[text()='Площадь кухни:']/following-sibling::div[@class='description']").InnerText;
                advert.Square = double.Parse(digitRegex.Match(squareText).Value);
                advert.LivingSquare = double.Parse(digitRegex.Match(livingText).Value);
                advert.LivingSquare = double.Parse(digitRegex.Match(kitchenText).Value);
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }
    
    }
}
