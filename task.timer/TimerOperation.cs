﻿using common.shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using unity.eventbroker;
using unity.eventbrokerextension;

namespace task.timer
{
    public class TimerOperation 
    {
        [Publishes(EventName.OnStartService)]
        public event TaskEventHandler OnInitialize;

        public async Task Initialize(TimerTaskPool pool, Dictionary<Type, ITimerTaskConfig> configuration)
        {
            await OnInitialize(this, new TimerEventArgs(pool, configuration));
        }

        public static void AddTask(EventArgs eventArgs, Type type, Func<Task> action, string taskName = null)
        {
            if (!(eventArgs is TimerEventArgs serviceEventArgs)) return;

            if (!TryGetTaskConfiguration(eventArgs, type, out var taskConfig)) return;

            if (!int.TryParse(taskConfig.Interval, out var interval)) interval = 60;

            serviceEventArgs.TasksPool.AddTask(new TimerTask($"{taskConfig.Type}{taskName ?? string.Empty}", interval, action));
        }

        public static int? GetInterval(string taskName, List<KeyValuePair<string, int>> typeTasks)
        {
            var typeTask = typeTasks.FirstOrDefault(t => t.Key.Equals(taskName, StringComparison.InvariantCultureIgnoreCase));
            if (typeTask.Equals(default(KeyValuePair<string, int>))) return null;

            return typeTask.Value;

        }

        public static bool TryGetTaskConfiguration(EventArgs eventArgs, Type type, out ITimerTaskConfig taskConfiguration)
        {
            taskConfiguration = null;
            var serviceEventArgs = eventArgs as TimerEventArgs;
            if (serviceEventArgs == null) return false;

            var configuration = serviceEventArgs.Configuration;
            if (!configuration.ContainsKey(type)) return false;

            taskConfiguration = configuration[type];

            return true;
        }
    }
}
