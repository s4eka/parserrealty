﻿
namespace task.timer
{
    public interface ITimerTaskConfig
    {
        string Type { get; set; }
        string Interval { get; set; }
    }
}
