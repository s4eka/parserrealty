﻿using System;
using System.Collections.Generic;

namespace task.timer
{
    public class TimerEventArgs : EventArgs
    {
        public TimerTaskPool TasksPool;
        public Dictionary<Type, ITimerTaskConfig> Configuration;

        public TimerEventArgs(TimerTaskPool tasksPool, Dictionary<Type, ITimerTaskConfig> configuration)
        {
            TasksPool = tasksPool;
            Configuration = configuration;
        }
    }
}
