﻿using System.Collections.Generic;
using System.Configuration;

namespace task.timer
{
    public  class TaskConfigSection : ConfigurationSection
    {
        [ConfigurationProperty("Tasks")]
        public TaskCollection TaskItems => ((TaskCollection)(base["Tasks"]));
    }

    [ConfigurationCollection(typeof(TimerTaskConfigElement), AddItemName = "Task")]
    public class TaskCollection : ConfigurationElementCollection
    {
       protected override ConfigurationElement CreateNewElement()
        {
            return new TimerTaskConfigElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return element;
        }

        public TimerTaskConfigElement this[int idx] => (TimerTaskConfigElement)BaseGet(idx);

        public new IEnumerator<TimerTaskConfigElement> GetEnumerator()
        {
            var count = base.Count;
            for (var i = 0; i < count; i++)
            {
                yield return base.BaseGet(i) as TimerTaskConfigElement;
            }
        }
    }
}
