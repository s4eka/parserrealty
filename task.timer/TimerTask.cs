﻿using System;
using System.Threading;
using System.Threading.Tasks;
using logging.core;

namespace task.timer
{
    public class TimerTask
    {
        private object _lock = new object();
        public string Name { get; private set; }
        public int ExecutionIntervalSeconds { get; private set; }
        public DateTime LastRun { get; private set; }
        public bool InProgress { get; set; }
        public Func<Task> RunMethod { get; set; }
        private ILogContext _logger { get; set; }

        public DateTime NextRun => this.LastRun.AddSeconds(this.ExecutionIntervalSeconds);

        private Thread _thread { get; set; }

        public TimerTask(string name, int executionIntervalSeconds, Func<Task> runMethod)
        {
            this.Name = name;
            this.ExecutionIntervalSeconds = executionIntervalSeconds;
            this.RunMethod = runMethod;
            this.LastRun = DateTime.MinValue;
            this.InProgress = false;
        }

        public void Run(ILogContext logContext, CancellationToken token)
        {
            var now = DateTime.UtcNow;
            _logger = logContext;

            lock (_lock)
            {
                if (InProgress || NextRun > now) return;
                this.InProgress = true;
                Task.Run(Execute, token);
            }
        }

        //public void Stop()
        //{
        //    _thread?.Abort();
        //}

        public async Task Execute()
        {
            using (new LogHelper<ILogContext>(_logger, "task.started: {0}", Name))
            {
                try
                {
                    LastRun = DateTime.UtcNow;
                    await RunMethod();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    _logger?.Error($"Exception Happens in:{this.Name}", ex);
                }
                finally
                {
                    _logger?.InfoFormat("{0} Finished", this.Name);
                    this.InProgress = false;
                }
            }
        }
    }
}
