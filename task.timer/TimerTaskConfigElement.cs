﻿using System.Configuration;

namespace task.timer
{
    public class TimerTaskConfigElement : ConfigurationElement, ITimerTaskConfig
    {
        [ConfigurationProperty("type", DefaultValue = "", IsKey = true, IsRequired = true)]
        public string Type
        {
            get { return ((string)(base["type"])); }
            set { base["type"] = value; }
        }

        //[ConfigurationProperty("name", DefaultValue = "", IsKey = false, IsRequired = true)]
        //public string Name
        //{
        //    get { return ((string)(base["name"])); }
        //    set { base["name"] = value; }
        //}

        [ConfigurationProperty("interval", DefaultValue = "5", IsKey = false, IsRequired = true)]
        public string Interval
        {
            get { return ((string)(base["interval"])); }
            set { base["interval"] = value; }
        }
    }
}
