﻿using System;
using System.Collections.Generic;
using System.Threading;
using logging.core;

namespace task.timer
{
    public class TimerTaskPool
    {
        private Dictionary<string, TimerTask> _pool = new Dictionary<string, TimerTask>();
        private Thread _executer;
        private CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();

        private ILogContext Logger { get; set; }
        public TimerTaskPool(ILogContext logger)
        {
            Logger = logger;
        }

        public void AddTask(TimerTask task)
        {
            _pool[task.Name] = task;
        }

        /// <summary>
        /// Add task if enabled
        /// </summary>
        /// <param name="task"></param>
        /// <param name="enabled"></param>
        public void AddTask(TimerTask task, bool enabled)
        {
            if (enabled)
                _pool[task.Name] = task;
        }

        private void Execute()
        {
            while (!_cancellationTokenSource.Token.IsCancellationRequested)
                foreach (var item in _pool.Values)
                    item.Run(Logger, _cancellationTokenSource.Token);
        }

        public void Start()
        {
            _executer = new Thread(Execute) { IsBackground = true };
            _executer.Start();
        }
        public void Stop()
        {
            _cancellationTokenSource.Cancel();
        }
    }
}
