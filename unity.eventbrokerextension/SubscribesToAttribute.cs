﻿using System;

namespace unity.eventbrokerextension
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
    public class SubscribesToAttribute : PublishSubscribeAttribute
    {
        public int OrderNumber { get; set; }

        public SubscribesToAttribute(string eventName, int orderNumber = 0) : base(eventName)
        {
            OrderNumber = orderNumber;
        }
    }
}
