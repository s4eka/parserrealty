﻿using System.Collections.Generic;
using System.Reflection;

namespace unity.eventbrokerextension
{
    public class EventBrokerInfoPolicy : IEventBrokerInfoPolicy
    {
        private List<PublicationInfo> publications = new List<PublicationInfo>();
        private List<SubscriptionInfo> subscriptions = new List<SubscriptionInfo>();

        public void AddPublication(string publishedEventName, string eventName)
        {
            publications.Add(new PublicationInfo(publishedEventName, eventName));
        }

        public void AddSubscription(string publishedEventName, MethodInfo subscriber, int orderNumber)
        {
            subscriptions.Add(new SubscriptionInfo(publishedEventName, subscriber, orderNumber));
        }

        public IEnumerable<PublicationInfo> Publications
        {
            get { return publications; }
        }

        public IEnumerable<SubscriptionInfo> Subscriptions
        {
            get { return subscriptions; }
        }
    }
}
