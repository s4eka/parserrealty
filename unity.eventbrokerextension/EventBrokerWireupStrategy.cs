﻿using System;
using unity.eventbroker;
using Unity.Builder;
using unity.extension;
using Unity.Strategies;
using Unity.Policy;

namespace unity.eventbrokerextension
{
    public class EventBrokerWireupStrategy : BuilderStrategy
    {
        public override void PreBuildUp(ref BuilderContext context)
        {
            if (context.Existing != null)
            {
                IEventBrokerInfoPolicy policy =
                     (IEventBrokerInfoPolicy)context.List.Get(context.Type, context.Name, typeof(IEventBrokerInfoPolicy));
                if (policy != null)
                {
                    EventBroker broker = GetBroker(context);
                    foreach (PublicationInfo pub in policy.Publications)
                    {
                        broker.RegisterPublisher(pub.PublishedEventName, context.Existing, pub.EventName);
                    }
                    foreach (SubscriptionInfo sub in policy.Subscriptions)
                    {
                        broker.RegisterSubscriber
                            (
                                sub.PublishedEventName,
                                (TaskEventHandler)Delegate.CreateDelegate
                                    (
                                        typeof(TaskEventHandler),
                                        context.Existing,
                                        sub.Subscriber
                                    ),
                                sub.OrderNumber
                            );
                    }
                }
            }
        }

        private EventBroker GetBroker(BuilderContext context)
        {
            var broker = context.Container.NewBuildUp2<EventBroker>();
            if (broker == null)
            {
                throw new InvalidOperationException("No event broker available");
            }
            return broker;
        }
    }
}



