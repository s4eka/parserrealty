﻿using System;
using System.Reflection;
using Unity.Builder;
using Unity.Policy;
using Unity.Strategies;

namespace unity.eventbrokerextension
{
    public class EventBrokerReflectionStrategy : BuilderStrategy
    {
        public EventBrokerReflectionStrategy() : base()
        {
        }

        public override void PreBuildUp(ref BuilderContext context)
        {
            if (context.Registration.Get<IEventBrokerInfoPolicy>() == null)
            {
                EventBrokerInfoPolicy policy = new EventBrokerInfoPolicy();
                context.List.Set(context.Type, context.Name, typeof(IEventBrokerInfoPolicy), policy);

                AddPublicationsToPolicy(context, policy);
                AddSubscriptionsToPolicy(context, policy);
            }
        }

        private void AddPublicationsToPolicy(BuilderContext context, EventBrokerInfoPolicy policy)
        {
            Type t = context.RegistrationType;
            foreach (EventInfo eventInfo in t.GetEvents())
            {
                PublishesAttribute[] attrs =
                    (PublishesAttribute[])eventInfo.GetCustomAttributes(typeof(PublishesAttribute), true);
                if (attrs.Length > 0)
                {
                    policy.AddPublication(attrs[0].EventName, eventInfo.Name);
                }
            }
        }

        private void AddSubscriptionsToPolicy(BuilderContext context, EventBrokerInfoPolicy policy)
        {
            foreach (MethodInfo method in context.RegistrationType.GetMethods())
            {
                SubscribesToAttribute[] attrs = (SubscribesToAttribute[])method.GetCustomAttributes(typeof(SubscribesToAttribute), true);
                if (attrs.Length > 0)
                {
                    policy.AddSubscription(attrs[0].EventName, method, attrs[0].OrderNumber);
                }
            }
        }
    }
}
