﻿using System.Collections.Generic;
using System.Reflection;
using Unity.Policy;

namespace unity.eventbrokerextension
{
    /// <summary>
    /// This policy interface allows access to saved publication and
    /// subscription information.
    /// </summary>
    public interface IEventBrokerInfoPolicy
    {
        IEnumerable<PublicationInfo> Publications { get; }
        IEnumerable<SubscriptionInfo> Subscriptions { get; }
    }

    public struct PublicationInfo
    {
        public string PublishedEventName;
        public string EventName;

        public PublicationInfo(string publishedEventName, string eventName)
        {
            PublishedEventName = publishedEventName;
            EventName = eventName;
        }
    }

    public struct SubscriptionInfo
    {
        public string PublishedEventName;
        public MethodInfo Subscriber;
        public int OrderNumber;


        public SubscriptionInfo(string publishedEventName, MethodInfo subscriber, int orderNumber)
        {
            PublishedEventName = publishedEventName;
            Subscriber = subscriber;
            OrderNumber = orderNumber;
        }
    }
}
