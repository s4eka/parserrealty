﻿using System;

namespace unity.eventbrokerextension
{
    [AttributeUsage(AttributeTargets.Event, Inherited = true)]
    public class PublishesAttribute : PublishSubscribeAttribute
    {
        public PublishesAttribute(string publishedEventName) : base(publishedEventName)
        {
        }
    }
}
