﻿using unity.eventbroker;
using Unity;
using Unity.Builder;
using Unity.Extension;
using Unity.Lifetime;
using unity.extension;

namespace unity.eventbrokerextension
{
    public class EventBrokerExtension : UnityContainerExtension, IEventBrokerConfiguration
    {
        private readonly EventBroker broker = new EventBroker();

        protected override void Initialize()
        {
            Context.Container.RegisterInstance(broker, new ExternallyControlledLifetimeManager());

            Context.Strategies.AddNew2<EventBrokerReflectionStrategy, UnityBuildStage>(UnityBuildStage.PreCreation);
            Context.Strategies.AddNew2<EventBrokerWireupStrategy, UnityBuildStage>(UnityBuildStage.Initialization);
        }

        public EventBroker Broker
        {
            get { return broker; }
        }
    }
}
