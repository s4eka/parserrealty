﻿using unity.eventbroker;
using Unity.Extension;

namespace unity.eventbrokerextension
{
    public interface IEventBrokerConfiguration : IUnityContainerExtensionConfigurator
    {
        EventBroker Broker { get; }
    }
}
