namespace Unity.AutoRegistration
{
    /// <summary>
    /// Extension methods for fluent registration options
    /// </summary>
    public static class Then
    {
        /// <summary>
        /// Creates new registration options
        /// </summary>
        /// <returns>Fluent registration options</returns>
        public static IFluentRegistration Register()
        {
            return new RegistrationOptions().RegisterType();
        }

        /// <summary>
        /// Creates new registration options with autoresolve instruction
        /// </summary>
        /// <returns></returns>
        public static IFluentRegistration AutoResolve()
        {
            return new RegistrationOptions().AutoResolve();
        }
    }
}