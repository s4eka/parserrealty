﻿using System;
using System.Collections.Generic;
using System.Linq;
using Unity.Injection;
using Unity.Lifetime;
using Unity.Registration;

namespace Unity.AutoRegistration
{
    /// <summary>
    /// Type registration options
    /// </summary>
    public class RegistrationOptions : IFluentRegistration
    {
        private Type _type;

        private Func<Type, IEnumerable<Type>> _interfacesToRegisterAsResolver =
            t =>
                new List<Type>(
                    t.IsGenericType && t.GetInterfaces().Count(i => i.IsGenericType) > 0
                    ? t.GetInterfaces()
                        .Where(i => i.IsGenericType)
                        .ToList()
                        .ConvertAll(x => x.GetGenericTypeDefinition())
                        .ToArray()
                    : t.GetInterfaces());

        private Func<Type, string> _nameToRegisterWithResolver = t => null;
        private Func<Type, ITypeLifetimeManager> _lifetimeManagerToRegisterWithResolver = t => new TransientLifetimeManager();
        private Func<Type, InjectionMember[]> _injectionMembers = t => new InjectionMember[0];
        private bool _resolve = false;
        private bool _register = false;

        public IFluentRegistration AutoResolve()
        {
            _resolve = true;
            return this;
        }

        public IFluentRegistration RegisterType()
        {
            _register = true;
            return this;
        }

        /// <summary>
        /// Gets or sets lifetime manager to use to register type(s).
        /// </summary>
        /// <value>Lifetime manager.</value>
        public ITypeLifetimeManager ITypeLifetimeManager
        {
            get { return _lifetimeManagerToRegisterWithResolver(_type); }
            set { _lifetimeManagerToRegisterWithResolver = t => value; }
        }

        /// <summary>
        /// Gets or sets name to register type(s) with.
        /// </summary>
        /// <value>Name.</value>
        public string Name
        {
            get { return _nameToRegisterWithResolver(_type); }
            set { _nameToRegisterWithResolver = t => value; }
        }

        /// <summary>
        /// Gets or sets interfaces to register type(s) as.
        /// </summary>
        /// <value>Interfaces.</value>
        public IEnumerable<Type> Interfaces
        {
            get { return _interfacesToRegisterAsResolver(_type); }
            set { _interfacesToRegisterAsResolver = t => value; }
        }

        /// <summary>
        /// Gets or sets injection members collection to register type(s) with.
        /// </summary>
        /// <value>Interfaces.</value>
        public InjectionMember[] InjectionMembers
        {
            get { return _injectionMembers(_type); }
            set { _injectionMembers = t => value; }
        }

        /// <summary>
        /// Sets type being registered.
        /// </summary>
        /// <value>Target type.</value>
        public Type Type
        {
            set
            {
                if (value == null)
                    throw new ArgumentNullException();
                _type = value;
            }
        }

        public bool Resolve
        {
            get
            {
                return _resolve;
            }
        }

        public bool Register
        {
            get
            {
                return _register;
            }
        }

        /// <summary>
        /// Specifies lifetime manager to use when registering type
        /// </summary>
        /// <typeparam name="TLifetimeManager">The type of the lifetime manager.</typeparam>
        /// <returns>Fluent registration</returns>
        public IFluentRegistration UsingLifetime<TLifetimeManager>() where TLifetimeManager : ITypeLifetimeManager, new()
        {
            _lifetimeManagerToRegisterWithResolver = t => new TLifetimeManager();
            return this;
        }

        /// <summary>
        /// Specifies lifetime manager resolver function, that by given type return lifetime manager to use when registering type
        /// </summary>
        /// <param name="lifetimeResolver">Lifetime manager resolver.</param>
        /// <returns>Fluent registration</returns>
        public IFluentRegistration UsingLifetime(Func<Type, ITypeLifetimeManager> lifetimeResolver)
        {
            if (lifetimeResolver == null)
                throw new ArgumentNullException("lifetimeResolver");

            _lifetimeManagerToRegisterWithResolver = lifetimeResolver;
            return this;
        }

        /// <summary>
        /// Specifies lifetime manager to use when registering type
        /// </summary>
        /// <typeparam name="TLifetimeManager">The type of the lifetime manager.</typeparam>
        /// <param name="manager"></param>
        /// <returns>Fluent registration</returns>
        public IFluentRegistration UsingLifetime<TLifetimeManager>(TLifetimeManager manager)
            where TLifetimeManager : ITypeLifetimeManager
        {
            if (manager == null)
                throw new ArgumentNullException("manager");

            _lifetimeManagerToRegisterWithResolver = t => manager;
            return this;
        }

        /// <summary>
        /// Specifies ContainerControlledLifetimeManager lifetime manager to use when registering type
        /// </summary>
        /// <returns>Fluent registration</returns>
        public IFluentRegistration UsingSingletonMode()
        {
            _lifetimeManagerToRegisterWithResolver = t => new ContainerControlledLifetimeManager();
            return this;
        }

        /// <summary>
        /// Specifies TransientLifetimeManager lifetime manager to use when registering type
        /// </summary>
        /// <returns>Fluent registration</returns>
        public IFluentRegistration UsingPerCallMode()
        {
            _lifetimeManagerToRegisterWithResolver = t => new TransientLifetimeManager();
            return this;
        }

        /// <summary>
        /// Specifies PerThreadLifetimeManager lifetime manager to use when registering type
        /// </summary>
        /// <returns>Fluent registration</returns>
        public IFluentRegistration UsingPerThreadMode()
        {
            _lifetimeManagerToRegisterWithResolver = t => new PerThreadLifetimeManager();
            return this;
        }

        /// <summary>
        /// Specifies name to register type with
        /// </summary>
        /// <param name="name">Name.</param>
        /// <returns>Fluent registration</returns>
        public IFluentRegistration WithName(string name)
        {
            if (name == null)
                throw new ArgumentNullException("name");

            Name = name;
            return this;
        }

        /// <summary>
        /// Specifies name resolver function that by given type returns name to register it with
        /// </summary>
        /// <param name="nameResolver">Name resolver.</param>
        /// <returns>Fluent registration</returns>
        public IFluentRegistration WithName(Func<Type, string> nameResolver)
        {
            if (nameResolver == null)
                throw new ArgumentNullException("nameResolver");

            _nameToRegisterWithResolver = nameResolver;
            return this;
        }

        /// <summary>
        /// Specifies that type name should be used to register it with
        /// </summary>
        /// <returns>Fluent registration</returns>
        public IFluentRegistration WithTypeName()
        {
            _nameToRegisterWithResolver = t => t.Name;
            return this;
        }

        /// <summary>
        /// Specifies that type should be registered with its name minus well-known application part name.
        /// For example: WithPartName("Controller") will register 'HomeController' type with name 'Home',
        /// or WithPartName(WellKnownAppParts.Repository) will register 'CustomerRepository' type with name 'Customer'
        /// </summary>
        /// <param name="name">Application part name.</param>
        /// <returns>Fluent registration</returns>
        public IFluentRegistration WithPartName(string name)
        {
            _nameToRegisterWithResolver = t =>
            {
                var typeName = t.Name;
                if (typeName.EndsWith(name))
                    return typeName.Remove(typeName.Length - name.Length);
                return typeName;
            };
            return this;
        }

        /// <summary>
        /// Specifies interface to register type as
        /// </summary>
        /// <typeparam name="TContact">The type of the interface.</typeparam>
        /// <returns>Fluent registration</returns>
        public IFluentRegistration As<TContact>() where TContact : class
        {
            _interfacesToRegisterAsResolver = t => new List<Type> { typeof(TContact) };
            return this;
        }

        /// <summary>
        /// Specifies interface resolver function that by given type returns interface register type as
        /// </summary>
        /// <param name="typeResolver">Interface resolver.</param>
        /// <returns>Fluent registration</returns>
        public IFluentRegistration As(Func<Type, Type> typeResolver)
        {
            if (typeResolver == null)
                throw new ArgumentNullException("typeResolver");

            _interfacesToRegisterAsResolver = t => new List<Type> { typeResolver(t) };
            return this;
        }

        /// <summary>
        /// Specifies interface resolver function that by given type returns interfaces register type as
        /// </summary>
        /// <param name="typesResolver">Interface resolver.</param>
        /// <returns>Fluent registration</returns>
        public IFluentRegistration As(Func<Type, Type[]> typesResolver)
        {
            if (typesResolver == null)
                throw new ArgumentNullException("typesResolver");

            _interfacesToRegisterAsResolver = t => new List<Type>(typesResolver(t));
            return this;
        }

        /// <summary>
        /// Specifies that type should be registered as its first interface
        /// </summary>
        /// <returns>Fluent registration</returns>
        public IFluentRegistration AsFirstInterfaceOfType()
        {
            _interfacesToRegisterAsResolver = t => new List<Type> { t.GetInterfaces().First() };
            return this;
        }

        /// <summary>
        /// Specifies that type should be registered as its single interface
        /// </summary>
        /// <returns>Fluent registration</returns>
        public IFluentRegistration AsSingleInterfaceOfType()
        {
            _interfacesToRegisterAsResolver = t => new List<Type> { t.GetInterfaces().Single() };
            return this;
        }

        public IFluentRegistration AsBaseType()
        {
            _interfacesToRegisterAsResolver = t => new List<Type> { t.BaseType };
            return this;
        }

        /// <summary>
        /// Specifies that type should be registered as all its interfaces
        /// </summary>
        /// <returns>Fluent registration</returns>
        public IFluentRegistration AsAllInterfacesOfType()
        {
            _interfacesToRegisterAsResolver = t => new List<Type>(t.GetInterfaces());
            return this;
        }

        /// <summary>
        /// Specifies that type should be registered as its single interface That Implements TypeName without postfix
        /// </summary>
        /// <returns>Fluent registration</returns>
        public IFluentRegistration AsITypeNameWithoutPostfix(string postfix)
        {
            _interfacesToRegisterAsResolver =
                t =>
                    new List<Type>
                    {
                        t.GetInterfaces()
                            .Single(i => i.Name.StartsWith("I") && string.Concat(i.Name.Remove(0, 1), postfix) == t.Name)
                    };
            return this;
        }

        /// <summary>
        /// Specifies that type should be registered as its single interface That Implements TypeName with postfix
        /// </summary>
        /// <returns>Fluent registration</returns>
        public IFluentRegistration AsITypeNameWithPostfix(string postfix)
        {
            _interfacesToRegisterAsResolver =
                t =>
                    new List<Type>
                    {
                        t.GetInterfaces()
                            .Single(i => i.Name.StartsWith("I") && i.Name.Remove(0, 1) == string.Concat(t.Name, postfix))
                    };
            return this;
        }


        public IFluentRegistration AsITypeNameMatchesWithName(Func<Type, string> nameResolver)
        {
            _interfacesToRegisterAsResolver =
                 t =>
                 {
                     Name = nameResolver(t);
                     Type[] interfaces = t.IsGenericType && t.GetInterfaces().Count(i => i.IsGenericType) > 0
                         ? t.GetInterfaces().Where(i => i.IsGenericType).ToList().ConvertAll(x => x.GetGenericTypeDefinition()).ToArray()
                         : t.GetInterfaces();
                     return new List<Type>
                          {
                             interfaces.Single(i => i.Name.StartsWith("I") && i.Name.Remove(0, 1) == t.Name)
                          };

                 };
            return this;
        }

        public IFluentRegistration AsITypeNameMatches()
        {
           _interfacesToRegisterAsResolver =
                t =>
                {
                    Type[] interfaces = t.IsGenericType && t.GetInterfaces().Count(i => i.IsGenericType) > 0
                        ? t.GetInterfaces().Where(i => i.IsGenericType).ToList().ConvertAll(x => x.GetGenericTypeDefinition()).ToArray()
                        : t.GetInterfaces();
                    return new List<Type>
                         {
                             interfaces.Single(i => i.Name.StartsWith("I") && i.Name.Remove(0, 1) == t.Name)
                         };
                   
                };
            return this;
        }

        /// <summary>
        /// Specifies that type should be registered with injection members
        /// </summary>
        /// <returns>Fluent registration</returns>
        public IFluentRegistration WithInjection(InjectionMember[] members)
        {
            if (members == null)
                throw new ArgumentNullException("members");

            _injectionMembers = t => members;
            return this;
        }

        /// <summary>
        /// Specifies PerResolveLifetimeManager lifetime manager to use when registering type
        /// </summary>
        /// <returns>Fluent registration</returns>
        public IFluentRegistration UsingPerResolveMode()
        {
            _lifetimeManagerToRegisterWithResolver = t => new PerResolveLifetimeManager();
            return this;
        }
    }
}