using common.shared;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using task.timer;
using unity.infrastructure;
using Unity;
using logging.core;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using System.Threading;

namespace service.background.parser
{
    /// <summary>
    /// Service class
    /// </summary>
    partial class SchedulerService : IHostedService, IDisposable
    {
        private ILogContext _logContext;
        private TimerTaskPool _pool;
        //private readonly IList<ServiceHost> _serviceHosts = new List<ServiceHost>();

        /// <summary>
        /// Constructor
        /// </summary>
        public SchedulerService()
        {
            IUnityContainer container = Bootstrapper.Container;
            _logContext = container.Resolve<ILogContext>();

            var tasks = InitializeTasks();

            _pool = new TimerTaskPool(_logContext);
            var task = Task.Run(async () => await container.Resolve<TimerOperation>().Initialize(_pool, tasks));
            Task.WaitAll(task);

            InitializeServices();
        }

        private static Dictionary<Type, ITimerTaskConfig> InitializeTasks()
        {
            var subscribers = ConfigurationManager.GetSection(common.shared.Section.Tasks);
            if (null == subscribers) return new Dictionary<Type, ITimerTaskConfig>();

            var section = (TaskConfigSection)subscribers;
            var elements = section.TaskItems;
            var dictionary = new Dictionary<Type, ITimerTaskConfig>();
            foreach (var element in elements)
            {
                var type = Type.GetType(element.Type);
                if (!dictionary.ContainsKey(type)) dictionary.Add(type, element);
            }
            return dictionary;
        }

        private void InitializeServices()
        {
            var services = (NameValueCollection)ConfigurationManager.GetSection(Section.Services);
            if (null == services) return;

            foreach (var service in services.AllKeys)
            {
                // need update
                //_serviceHosts.Add(new DependencyInjectionServiceHost(Type.GetType(service)));
            }
        }

        public void Dispose()
        {
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            _logContext.Trace("Start service");
            await Task.Run(() => _pool.Start(), cancellationToken);
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            _pool.Stop();
            _logContext.Trace("Wait for the service to end safely...");
        }
    }
}
