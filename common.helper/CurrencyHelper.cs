﻿using System;
using System.Collections.Generic;
using System.Text;

namespace common.helper
{
    public class CurrencyHelper
    {
        public static Dictionary<string, string> Currencies = new Dictionary<string, string>()
        {
            { "бел. руб","BYN" },
            { "$", "USD" },
            { "долл","USD" },
            { "руб","BYN" }
        };
    }
}
