﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace common.helper
{
    public static class EntityUpdateHelper
    {
        public static T GetUpdateParametr<T>(T obj1, T obj2)
        {
            if (NullNewEquals(obj1, obj2))
                return obj2;
            else
                return obj1;
        }


        public static bool NullNewEquals(object obj1, object obj2)
        {
            if (obj1 == null && obj2 != null) return true;
            return false;
        }

        public static bool NullEquals(object obj1, object obj2)
        {
            if ((obj1 == null) || (obj2 == null)) return true;
            if (obj1.Equals(obj2)) return true;
            return false;
        }
    }
}
