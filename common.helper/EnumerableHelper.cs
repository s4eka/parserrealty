﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace common.helper
{
    public static class EnumerableHelper
    {
        public static async Task ForEachAsync<T>(this IEnumerable<T> enumerable, Action<T> action)
        {
            foreach (var item in enumerable)
                await Task.Run(() => { action(item); }).ConfigureAwait(false);
        }

        public static async Task<IEnumerable<T1>> SelectManyAsync<T, T1>(this IEnumerable<T> enumeration, Func<T, Task<IEnumerable<T1>>> func)
        {
            return (await Task.WhenAll(enumeration.Select(func))).SelectMany(s => s);
        }

    }
}
