﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace common.helper
{
    public static class TaskHelper
    {
        public static IEnumerable<TOut> AsParallel<TIn, TResult, TOut>(this IEnumerable<TIn> enumerable, Func<TIn, TResult> process, Func<TIn, TResult, TOut> result)
        {
            //var tasks = new List<Task<Tuple<TIn, TResult>>>();
            //tasks.AddRange(enumerable.Select(element => new Task<Tuple<TIn, TResult>>(() => new Tuple<TIn, TResult>(element, process(element)))));
            //Parallel.ForEach(tasks, task => { task.Start(); });
            //return tasks.Select( task => result(task.Result.Item1, task.Result.Item2));

            var tasks = new List<Task<Tuple<TIn, TResult>>>();
            tasks.AddRange(enumerable.Select(element => Task.Factory.StartNew(() => new Tuple<TIn, TResult>(element, process(element)))));
            Task.WaitAll(tasks.ToArray());
            return tasks.Select(task => result(task.Result.Item1, task.Result.Item2));
        }

    }
}
