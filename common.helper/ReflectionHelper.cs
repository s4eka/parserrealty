﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace common.helper
{
    public static class ReflectionHelper
    {
        /// <summary>
        /// For example, if property is called "model.Abc.Def" it returns "Def"
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="memberAccess"></param>
        /// <returns></returns>
        public static string GetLastPartOfMemberName<TKey, TValue>(Expression<Func<TKey, TValue>> memberAccess)
        {
            return ((MemberExpression) memberAccess.Body).Member.Name;
        }

        /// <summary>
        /// For example, if property is called "model.Abc.Def" it returns "Abc.Def"
        /// </summary>
        /// <param name="memberAccess"></param>
        /// <returns></returns>
        public static string GetRightPartOfMemberName<TKey, TValue>(Expression<Func<TKey, TValue>> memberAccess)
        {
            string path = string.Empty;
            MemberExpression memberExpression;

            if (memberAccess.Body is UnaryExpression)
            {
                UnaryExpression unaryExpression = (UnaryExpression) memberAccess.Body;

                if (unaryExpression.NodeType == ExpressionType.Convert)
                {
                    memberExpression = (MemberExpression) unaryExpression.Operand;

                    return memberExpression.Member.Name;
                }
            }

            memberExpression = (MemberExpression) memberAccess.Body;
            MemberExpression originalMemberExpression = memberExpression;

            while (memberExpression != null && memberExpression.Expression.NodeType == ExpressionType.MemberAccess)
            {
                var propertyInfo = memberExpression.Expression.GetType().GetProperty("Member");
                var propertyValue = propertyInfo.GetValue(memberExpression.Expression, null) as PropertyInfo;
                if (propertyValue != null)
                    path = propertyValue.Name + "." + path;

                memberExpression = memberExpression.Expression as MemberExpression;
            }

            return path + originalMemberExpression.Member.Name;
        }
    }
}
