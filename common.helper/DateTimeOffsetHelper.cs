﻿using System;

namespace common.helper
{
    public static class DateTimeOffsetHelper
    {
        public static long ToUnixTimeSeconds(this DateTimeOffset dateTime)
        {
            long num = dateTime.UtcDateTime.Ticks / 0x989680L;
            return (num - 0xe7791f700L);
        }

        public static double ToUnixTimestamp(this DateTime date)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan diff = date.ToUniversalTime() - origin;
            return Math.Floor(diff.TotalSeconds);
        }
    }
}
